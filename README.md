# Workshop IdentityServer 4

Instructions and source code for the KüberNerds IdentityServer 4 workshop

# Before you begin...
- Make sure you have the ASP.NET Core 3 **Preview 5** installed, or download it [here](https://dotnet.microsoft.com/download/dotnet-core/3.0).
- Make sure you have the latest Visual Studio 2019 Preview installed.
- Make sure you have Microsft SQl Server installed and know your credentials

# Scaffold IdentityServer 4
- First we have to install the IdentityServer 4 templates, run `dotnet new -i identityserver4.templates` in a terminal.
- Create a new folder for this workshop and run `dotnet new is4aspid -o IdentityServer` inside that folder.
- Now add a solution file and add the above created project `dotnet new sln && dotnet sln add ./IdentityServer`

# Upgrade the project to ASP.NET Core 3.0 Preview 5
- Open the solution in Visual Studio 2019 Preview
- Change the project to target `netcoreapp3.0`.
- Update all packages to the latest _preview_.
- Add the following packages (use the latest available previews)
    - `Newtonsoft.JSON`
    - `Microsoft.EntityFrameworkCore.SqlServer`
    - `Microsoft.AspNetCore.Identity.EntityFrameworkCore`
- Edit `Startup.cs`
    - Change `services.AddMvc()` to `services.AddMvc(options => options.EnableEndpointRouting = false)`
    - Remove 
    ```c#
    services.AddAuthentication()
        .AddGoogle(options =>
        {
            options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

            // register your IdentityServer with Google at https://console.developers.google.com
            // enable the Google+ API
            // set the redirect URI to http://localhost:5000/signin-google
            options.ClientId = "copy client ID from Google here";
            options.ClientSecret = "copy client secret from Google here";
        });
    ```
    - Remove `app.UseDatabaseErrorPage();`

# Use Microsoft SQL Server
- Create a new database in Sql Server, for example `IdentityServer`.
- In `Startup.cs` and `SeedData.cs` change `UseSqlite` to `UseSqlServer`.
- Change the connection string in `appsettings.json`, for example:
    ```
    Data Source=127.0.0.1;Initial Catalog=IdentityServer;Persist Security Info=True;User ID=sa;Password=<YourStrong!Passw0rd>
    ```
- If you have installed `dotnet-ef`, run ` dotnet tool uninstall --global dotnet-ef`.
- Run `dotnet tool install --global dotnet-ef --version 3.0.0-preview5.19227.1`
- In the folder `IdentityServer`, run the following command in a terminal `dotnet ef database update`

# Add a registration page
- Add a file named `RegisterViewModel.cs` with the following code
    ```c#
    using System.ComponentModel.DataAnnotations;

    namespace IdentityServer.Models
    {
        public class RegisterViewModel
        {
            [Required, MaxLength(256)]
            public string Username { get; set; }

            [Required, DataType(DataType.Password)]
            public string Password { get; set; }

            [DataType(DataType.Password), Compare(nameof(Password))]
            public string ConfirmPassword { get; set; }
        }
    }
    ```

- Create a folder named `Controllers`
- Create a file named `Account` in the folder `Controllers` and add the following code
    ```c#
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using IdentityServer.Models;

    namespace IdentityServerAspNetIdentity.Controllers
    {
        public class AccountController : Controller
        {
            private UserManager<ApplicationUser> _userManager;

            public AccountController(UserManager<ApplicationUser> userManager)
            {
                _userManager = userManager;
            }

            [HttpGet]
            public ViewResult Register()
            {
                return View();
            }

            [HttpPost]
            public async Task<IActionResult> Register(RegisterViewModel model)
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser { UserName = model.Username };
                    var result = await _userManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError("", error.Description);
                        }
                    }
                }
                return View();
            }
        }
    }
    ```

- Create a file named `Register.cshtml` in the folder Views/Account with the following code
    ```c#
    @model IdentityServer.Models.RegisterViewModel
    @{
        ViewBag.Title = "Register";
    }
    <div>
        <div class="page-header">
            <h1>Register</h1>
        </div>

        <div class="col-sm-6">

            <form method="post" asp-controller="Account" asp-action="Register">
                <div asp-validation-summary="ModelOnly"></div>

                <div class="form-group">
                    <label asp-for="Username"></label>
                    <input asp-for="Username" class="form-control" />
                    <span asp-validation-for="Username"></span>
                </div>

                <div class="form-group">
                    <label asp-for="Password"></label>
                    <input asp-for="Password" class="form-control" />
                    <span asp-validation-for="Password"></span>
                </div>

                <div class="form-group">
                    <label asp-for="ConfirmPassword"></label>
                    <input asp-for="ConfirmPassword" class="form-control" />
                    <span asp-validation-for="ConfirmPassword"></span>
                </div>

                <div>
                    <input type="submit" value="Register" />
                </div>
            </form>
        </div>
    </div>
    ```
- In `Views/Account/Login.cshtml` remove 
    ```html
    <div>
        <p>The default users are alice/bob, password: Pass123$</p>
    </div>
    ```
    and add
    ```html
    <div>
        <p><a href="/Account/Register">Register</a></p>
    </div>
    ```

# Add a client
- In `Config.cs`, change the "MVC client" config to the following
    ```c#
    new Client
    {
        ClientId = "mvc",
        ClientName = "MVC Client",

        AllowedGrantTypes = GrantTypes.Implicit,
        ClientSecrets = { new Secret("49C1A7E1-0C79-4A89-A3D6-A37998FB86B0".Sha256()) },

        RedirectUris = { "https://localhost:44307/signin-oidc" },
        FrontChannelLogoutUri = "https://localhost:44307/signout-oidc",
        PostLogoutRedirectUris = { "https://localhost:44307/signout-callback-oidc" },

        AllowOfflineAccess = true,
        AllowedScopes = { "openid", "profile", "api1" }                
    },
    ```

- Run the following command in the folder that the solution file lives, `dotnet new mvc -o InsideAirbnb && dotnet sln add ./InsideAirbnb`
- Add `Microsoft.AspNetCore.Authentication.OpenIdConnect` to the InsideAirbnb project
- Paste the following code beneath `services.AddRazorPages();`
    ```c#
    JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

    services.AddAuthentication(options =>
    {
        options.DefaultScheme = "Cookies";
        options.DefaultChallengeScheme = "oidc";
    })
        .AddCookie("Cookies")
        .AddOpenIdConnect("oidc", options =>
        {
            options.SaveTokens = true;
            options.ClientId = "mvc";
            options.ClientSecret = "49C1A7E1-0C79-4A89-A3D6-A37998FB86B0";
            options.RequireHttpsMetadata = false;
            options.Authority = "http://localhost:5000/";
        });
    ```
- Add `app.UseAuthentication();` after `app.UseRouting();`
- Put the `[Authorize]` attribute above the `Privacy` method in `HomeController.cs`; 

# Some notes

- To logout add a route with the following code `HttpContext.Authentication.SignOutAsync("oidc");`

- To define user roles, insert them in the `dbo.AspNetRoles` table